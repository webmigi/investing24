export const state = () => ({
  animation: true,
  step: 0,
  minStep: 0,
  maxStep: 7,
  cursor: "",
  breakpoints: {
    medium: 1024,
    tablet: 768,
    small: 450
  }
});

export const mutations = {
  TOGGLE_ANIMATION(state, val) {
    state.animation = val;
  },
  RESET_STEP(state, num) {
    state.step = num;
  },
  UP_STEP(state) {
    if (state.step < state.maxStep) {
      state.step++;
    }
  },
  DOWN_STEP(state) {
    if (state.step > state.minStep) {
      state.step--;
    }
  },
  TOGGLE_CURSOR(state, val) {
    state.cursor = val;
  }
};

export const actions = {
  RESET_STEP(context, val) {
    context.commit("RESET_STEP", val);
  },
  UP_STEP(context) {
    context.commit("UP_STEP");
  },
  DOWN_STEP(context) {
    context.commit("DOWN_STEP");
  },
  TOGGLE_ANIMATION(context, val) {
    context.commit("TOGGLE_ANIMATION", val);
  },
  TOGGLE_CURSOR(context, val) {
    context.commit("TOGGLE_CURSOR", val);
  }
};
