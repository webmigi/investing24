export default {
  data() {
    return {
      start: 0,
      end: 1,
      delay: 1000
    };
  },
  methods: {
    upCounter() {
      let self = this;
      let animationStart = setInterval(() => {
        self.start++;
        if (self.start >= self.end) {
          clearInterval(animationStart);
        }
      }, self.delay);
    },
    downCounter() {
      let self = this;
      let animationStart = setInterval(() => {
        self.start--;
        if (self.start === 0) {
          clearInterval(animationStart);
        }
      }, self.delay);
    }
  }
};
